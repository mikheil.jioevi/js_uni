<?php
    session_start();
    if(isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true){

    include "header.php";

    include "./connection/config.php";


    $query = "SELECT * FROM houses WHERE userID = '$_SESSION[user_id]'";
    $result = $conn->query($query);
    $row = $result -> fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/every.css">
    <link rel="stylesheet" href="styles/profile.css">
    <title>Document</title>
</head>
<body>
    
    <div class="profile">
        <div class="container">
            <div class="profile-inner">
                <h1>Hello <?=$_SESSION['username']?></h1>

                <div class="profile-houses">
                    <h1>your houses</h1>

                    <div class="houses-boxes">
                        <?php if($row){ ?>
                            <?php while ($row = $result->fetch_assoc()): ?>
                            <div class="box">
                                <img src="<?=$row['img_url']?>" alt="House Image">
                                <div class="box-content">
                                    <h3><?=$row['title']?></h3>
                                    <p><?=$row['content']?></p>
                                    <a href="singleHouse.php?id<?=$row['ID']?>" ><button>Show more</button></a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <?php }else { ?>
                            <h1>Nothing to fetch!</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>


<?php
    }else{
        header("Location: signin.php");
    }
?>