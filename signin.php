<?php
    include "./authentication/login.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/every.css">
    <link rel="stylesheet" href="styles/signin.css">
    <title>Document</title>
</head>
<body>

    <div class="login">
        <div class="container">
            <div class="login-inner">
                <h1>Sign in</h1>
                <form action="" method="post">
                    <p>Email</p>
                    <input type="email" name="email" id="">
                    <p>password</p>
                    <input type="password" name="password" id="">
                    <div class="login-btns">
                        <button>Sign in</button>
                        <a href="register.php">Doesn't have account?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>