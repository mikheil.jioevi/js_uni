var theme = true;

const changeTheme = () => {
  let header = document.getElementsByTagName("header");
  let body = document.getElementsByTagName("body");

  if (theme) {
    theme = !theme;
    header[0].classList.add("dark");
    body[0].classList.add("dark");
  } else {
    theme = !theme;
    header[0].classList.remove("dark");
    body[0].classList.remove("dark");
  }
};
