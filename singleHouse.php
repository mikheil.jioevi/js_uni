<?php
    include "./connection/config.php";
    include "./header.php";

    $postId = $_GET["id"];

    $query = "SELECT * FROM houses INNER JOIN comments ON houses.ID = comments.houseID WHERE houses.ID = '$postId' ";
    $result = $conn -> query($query);
    $row = mysqli_fetch_assoc($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/every.css">
    <link rel="stylesheet" href="styles/singlePost.css">
    <title>Document</title>
</head>
<body>
    <?php if ($row) { ?>
        <div class="single-post">
        <div class="container">
            <div class="single-inner">
                <h1><?=$row["title"]?></h1>
                <img src="<?=$row['img_url']?>" alt="">
                <p>Content:</p>
                <p><?=$row['content']?></p>

                <div class="comments">
                    <h1>Comments:</h1>
                    <p><?=$row['comment'] ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php }  else {
        echo "Error: No data found for the provided post ID.";
    }
    ?>
    
</body>
</html>