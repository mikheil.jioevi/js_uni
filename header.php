<link rel="stylesheet" href="styles/header.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<?php
    session_start();
?>

<header>
    <div class="container">
        
        <div id="theme" onClick=(changeTheme())>
            <i class="fa-solid fa-moon"></i>
        </div>
        
        <div class="header-inner">
            <div id="logo">
                <h1>Logo</h1>
            </div>

            <div class="navigation">
                <nav>
                    <ul>
                        <li><a href="profile.php">profile</a></li>
                    </ul>
                </nav>
            </div>

            <?php
                if(isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true){
            ?>
                <button onClick="logout()">Sign out</button>
            <?php
                }elseif($_SESSION['role'] == "admin"){
            ?>
                <button>Admin panel</button>
            <?php
                }else{
            ?>
            <div class="btns">
                    <a href="signin.php"><button>Sign in</button></a>
                    <a href="register.php"><button>Sign up</button></a>
                </div>
            <?php } ?>
        </div>
    </div>
</header>

<script>
    const logout = () => {
        window.location = "./authentication/logout.php";
    }
</script>
<script src="./theme.js"></script>