<?php
session_start();

include "./connection/config.php";

$emailErr = $passErr = $passMatchErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $username = $_POST["username"];
    $password = $_POST["password"];
    $passwordMatch = $_POST["passwordMatch"];

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Invalid email format";
    }

    if (strlen($password) <= 6) {
        $passErr = "Length of password must be longer than 6!";
    }


    if ($password !== $passwordMatch) {
        $passMatchErr = "Passwords do not match!";
    }

    if (empty($emailErr) && empty($passErr) && empty($passMatchErr)) {

        if ($conn->connect_error) {
            die("Connection error: " . $conn->connect_error);
        } else {
            $query = "INSERT INTO Users (email, username, password, role) VALUES ('$email', '$username', '$password', 'user')";
            $result = $conn->query($query);
            if ($result === TRUE) {
                $row = $result -> fetch_assoc();
                $_SESSION['loggedIn'] = true;
                $_SESSION['user_id'] = $row['id'];
                $_SESSION['username'] = $row['username'];
                $_SESSION['email'] = $row['email'];
                $_SESSION['role'] = $row['role'];
                header("Location: main.php");
                exit;
            } else {
                echo "Error: " . $query . "<br>" . $conn->error;
            }
        }
    }
}
?>