<?php
include "./authentication/registration.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/every.css">
    <link rel="stylesheet" href="styles/signPage.css">
    <title>Document</title>
</head>
<body>
    
    <div class="registration">
        <div class="container">
            <div class="regist-inner">
                <h1>Registrer</h1>
                <form action="" method="post">
                    <p>Email</p>
                    <input type="email" name="email">
                    <p class="ErorrMessage"><?=$emailErr?></p>
                    <p>Username</p>
                    <input type="text" name="username">
                    <p>password</p>
                    <input type="password" name="password">
                    <p class="ErorrMessage"><?=$passErr?></p>
                    <p>password</p>
                    <input type="password" name="passwordMatch">
                    <p class="ErorrMessage"><?=$passMatchErr?></p>
                    <div class="form-btns">
                        <button>Register</button>
                        <a href="signin.php">Already have an account?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>