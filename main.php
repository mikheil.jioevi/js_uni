<?php
    include "connection/config.php";

    $query = "SELECT * FROM houses";
    $result = $conn -> query($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles/every.css">
    <link rel="stylesheet" href="styles/main.css">
    <title>Document</title>
</head>
<body>

    <?php include "header.php"?>

    <div class="main">
        <div class="main-pro">
            <div class="container">
                <div class="main-pro_inner">
                    <div class="main-pro__content">
                        <h1>Buy your house here</h1>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum quo aperiam, labore natus explicabo in.</p>
                    </div>

                    <div class="search"></div>
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="flexible">
            <div class="container">
                <div class="flexible-inner">

                    <div class="flexible-inner__content">
                        <div class="imgs">
                            <img src="./imgs/flexible1.png" alt="">
                            <img src="./imgs/flexible2.png" alt="">
                            <img src="./imgs/flexible3.png" alt="">
                            <img src="./imgs/flexible4.png" alt="">
                        </div>

                        <div class="text">
                            <h1>The future is flexible</h1>
                            <p>We believe in a world where finding a home is just a click away. 
                                Whether you’re selling your home, travelling for work or moving to 
                                a new city.  Just bring your bags, and we’ll handle the rest.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="housList">
            <div class="container">
                <div class="housList-inner">
                    <?php while ($row = $result->fetch_assoc()):?>
                        <div class="houseList-box">
                            <img src="<?=$row['img_url']?>" alt="">
                            <div class="houseList-content">
                                <h3><?=$row["title"]?></h3>
                                <p><?=$row["content"]?></p>
                                <a href="singleHouse.php?id=<?=$row['ID']?> "><button>Show more</button></a>
                            </div>
                        </div>
                    <?php endwhile;?>
                </div>
            </div>
        </div>
    </div>
    
</body>
</html>